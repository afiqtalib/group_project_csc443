<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
    error_reporting(0);
    include 'con_to_db.php';
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Registration</title>
    <style>
        body{
            background-color: #00a2ff8f;
        }
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 110px 200px;
            row-gap: 5%;
            align-items: center;
        }
        .wrapper {
            background-color: #cef1f791;
        }
    </style>
</head>
<body>
    <br>
    <h1>Student Registration</h1>
    <div class="wrapper">
        <br> 
    <form action="add_student_handle.php" method="POST" >
        <div class="grid-container">
            <label class="grid-item grid-item-1" for="name"> Name: </label>
            <input type="text" name="l_name" class="grid-item grid-item-2" id="name" required> 

            <label for="matric">Matric Number: </label>
            <input type="num" name="matric" class="grid-item grid-item-4" id="matric" required>

            <label class="grid-item grid-item-3" for="email">Email: </label>
            <input type="email" name="email" class="grid-item grid-item-4" id="email" required>

            <label for="password">Password: </label>
            <input type="password" name="password" id="password" required>

            <label for="phone">Phone Number: </label>
            <input type="text" name="phone" id="phone" required>

            <label for="kelas">Class Name:</label>

               
            <select id="kelas" name="kelas">
                    <option value="" disabled selected>SELECT YOUR CLASS NAME</option> 
                    <?php $result = mysqli_query($conn,"select CLASS_NAME FROM CLASS");
                        while($row = mysqli_fetch_row($result)) {
                            foreach ($row as $cell) {
                                echo "<option value=\"$cell\">".strtoupper($cell)."</option>";
                            }} ?>   
            <input type="submit" style="margin-top: 10px;">
        </div>
        <br> <br> <br>
    </form>
    </div>
</body>
</html>