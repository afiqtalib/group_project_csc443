<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecturer Registration</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 110px 200px;
            row-gap: 5%;
        }
    </style>
</head>
<body>
    <br>
    <h1>Lecturer Registration</h1>
    <form action="add_lect.php" method="POST" >
        <div class="grid-container">
            <label class="grid-item grid-item-1" for="name"> Name: </label>
            <input type="text" name="l_name" class="grid-item grid-item-2" id="name" required> 

            <label class="grid-item grid-item-3" for="email">Email: </label>
            <input type="email" name="email" class="grid-item grid-item-4" id="email" required>

            <label for="password">Password: </label>
            <input type="password" name="password" id="password" required>

            <label for="phone">Phone Number: </label>
            <input type="text" name="phone" id="phone" required>
            <label></label>
            <input type="submit" style="margin-top: 10px;">
        </div>
    </form>
</body>
</html>