<!DOCTYPE html>
<html lang="en">
<head>
<title>SUBMISSION ASSIGNMENT SYSTEM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
  color: #515153;
}

/* Style the header */
header {
  background-color: rgb(4, 163, 255);
  padding: 30px;
  text-align: center;
  font-size: 35px;
  color: rgb(255, 255, 255);
}

/* Create two columns/boxes that floats next to each other */
nav {
  float:left;
  width: 20%; /* 30% */
  height: 300px; /* only for demonstration, should be removed */
  background: rgb(241, 241, 241);
  padding: 0px; /* 20px*/ 
}

/* Style the list inside the menu */
nav ul {
  list-style-type: none;
  padding: 0;
}
a:link, a:visited {
  background-color: #f44336;
  color: white;
  padding: 20px 60px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  width: 270px; /*test 250px */
}

a:hover, a:active {
  background-color: red;
}

article {
  float: left;
  padding: 20px;
  width: 80%; /* 70% */ 
  background-color: #f1f1f1;
  height: 300px; /* only for demonstration, should be removed */
  background-image: url(uitm.jpg);
  background-repeat: no-repeat;
  background-size: 500px;
  background-position: 100%;
}

/* Clear floats after the columns */
section::after {
  content: "";
  display: table;
  clear: both;
}

/* Style the footer */
footer {
  background-color: rgb(4, 162, 255);
  padding: 10px;
  font-size: 20px;  /*test */
  text-align: center;
  color: rgb(255, 255, 255);
}

/* Responsive layout - makes the two columns/boxes stack on top of each other instead of next to each other, on small screens */
@media (max-width: 600px) {
  nav, article {
    width: 100%;
    height: auto;
  }
}
  </style>
</head>
<body>


<header>
  <h2>SUBMISSION ASSIGNMENT SYSTEM</h2>
</header>
 
<section>
  <nav>
  <a href="create_group.php" target="_self">CREATE GROUP</a>
      <a href="to_submit.php" target="_self">SUBMISSION</a>
      <a href="logout.php" target="_self">LOGOUT</a>

  </nav>
   
  <article>
    <h1>WELCOME TO SUBMISSION ASSIGNMENT SYSTEM</h1>
  </article>
</section>

<footer>
  <p>UiTM CAWANGAN TERENGGANU KAMPUS KUALA TERENGGANU</p>
</footer>

</body>
</html>
