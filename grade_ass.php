<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Assignment List</title>
    <style>
        td { 
            text-align: center;
        }
        body {
            background-color: #a6eebb8a;
        }
    </style>
    <?php
    error_reporting(0);
    include 'con_to_db.php';
    session_start();
    if(isset($_SESSION["uid"])) $id = $_SESSION["uid"];
    else header("location:index.php"); ?>
</head>
<body>
    <?php include 'lect_nav.php'; ?>
    <br><br>
    <h2>ASSIGNMENT LIST</h2>
    <div>
        <form action="view_submission.php">
        <?php 
echo /*html*/ "<table border='1'><tr>
<th>Number</th>
<th>Assignment Name</th>
<th>Due Date</th>
<th>Assigned To</th></tr>";

$result = mysqli_query($conn,"select * from assignment where lect_id ='$id'");
$ctr=0;
while($row=mysqli_fetch_assoc($result))  {
    $idx[$ctr] = $ctr;    
    $ass_name[$ctr] = strtoupper($row["ASS_NAME"]);
    $ass_due[$ctr] = $row["ASS_DUE_DATE"];
    $ass_to[$ctr] = $row["CLASS_NUM"];
    $ctr++;
}
for ($i =0 ; $i < $ctr ; $i++ )
{
    echo "<tr>";
    echo /*html*/ "<td>". ($idx[$i] +1) ."</td>
    <td>$ass_name[$i]</td>
    <td>$ass_due[$i]</td>
    <td>$ass_to[$i]</td> ";
    echo "</tr>";
}
echo "</table";
mysqli_free_result($result);
?>
<br>
    </div>
<div>
    <label for="goto">GO TO ASSIGNMENT: </label>
                        <div>    
                            <select id="goto" name="goto">
                                <option value="0" disabled selected>SELECT ASSIGNMENT NAME</option> 
                                <?php $result = mysqli_query($conn,"select ASS_ID, ASS_NAME FROM assignment where lect_id = '$id'");
                        while($row = mysqli_fetch_row($result)) {
                            $i = 0;
                            foreach ($row as $cell) {
                                if($i % 2 == 0) {
                                    if (strcmp($cell, $id) != 0) echo "<option value=\"$cell\"";
                                    else $i+= 2;
                                }
                                else echo ">".strtoupper($cell)."</option>";
                                $i++;
                            }} ?>
                            </select>
                            <input type="submit">
                        </div>

    </form>

</div>
</body>
</html>