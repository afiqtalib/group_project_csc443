<!DOCTYPE html>
<html lang="en">
<head>
<?php 
error_reporting(0);
include 'con_to_db.php';
session_start();
?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Assignment</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 150px 300px;
            row-gap: 5%;
        }
        body {
            background-color: #a6eebb8a;
        }
    </style>
</head>
<body>
<?php include 'lect_nav.php'; ?>

<br>
    <h1>Create Assignment</h1>
    <form action="create_ass_handle.php" method="POST" enctype="multipart/form-data">
        <div class="grid-container">

            <label for="name"> Assignment Name: </label>
            <input type="text" name="name"  id="name" required> 

            <label  for="desc">Description  : </label>
            <textarea id="desc" name="desc" maxlength="290" rows="4" cols="50" placeholder="Enter decription here"></textarea>

            <label for="file">Upload an attachment:</label>
            <input type="file" id="file" name="file" >


            <label for="edDate">Due Date: </label>
            <input type="date" id="edDate" name="edDate"  required>
            
            <label for="edTime">Due Time: </label>
            <input type="time" id="edTime" name="edTime" required>

            <label for="kelas">Assign To:</label>
            <div>    
                <select id="kelas" name="kelas" required>
                    <option value="" disabled selected>SELECT CLASS</option> 
                    <?php $result = mysqli_query($conn,"select CLASS_NAME FROM CLASS where LECT_ID = '$_SESSION[uid]'");
                        while($row = mysqli_fetch_row($result)) {
                            foreach ($row as $cell) {
                                echo "<option value=\"$cell\">".strtoupper($cell)."</option>";
                            }} ?>
                </select>
                </div>
            <input type="submit" style="margin-top: 10px;">
        </div>
    </form>
</body>
</html>
