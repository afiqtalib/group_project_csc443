<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Class</title>
    <?php 
    error_reporting(0);
    include 'con_to_db.php';
    session_start();
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        //insert into db
        $sql = "INSERT INTO class( CLASS_NAME, LECT_ID) values ( '$_POST[name]', '$_SESSION[uid]')";
        if (!mysqli_query($conn, $sql)) {
            die('Error: '. $conn->error);
        }
        else {
            echo "<script>alert('Class Created');
          location.href = \"lecturerpage.php\";</script>";
        }
        
    }

    ?>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 150px 300px;
            row-gap: 5%;
        }
        body {
            background-color: #a6eebb8a;
        }
    </style>
</head>
<body>
<?php include 'lect_nav.php'; ?>

<br>
    <h1>Create Assignment</h1>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" >
        <div class="grid-container">

            <label for="name"> Class Name: </label>
            <input type="text" name="name"  id="name" required> 

            <input type="submit" style="margin-top: 10px;">
        </div>
    </form>
</body>
</html>
