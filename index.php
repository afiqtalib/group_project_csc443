<?php
error_reporting(0); 
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>SUBMISSION ASSIGNMENT SYSTEM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Style the header */
header {
  background-color: rgb(4, 163, 255);
  padding: 30px;
  text-align: center;
  font-size: 35px;
  color: rgb(230, 230, 230);
}

/* Create two columns/boxes that floats next to each other */
nav {
  float:left;
  width: 50%; /* 30% */
  height: 350px; /* only for demonstration, should be removed */
  background-color: #d2dcf3 ;
  padding:60px; /* 20px*/
   
}

/* Style the list inside the menu */
nav ul {
  list-style-type: none;
  padding: 0;
}
a:link, a:visited {
  background-color: #f44336;
  color: white;
  padding: 20px 60px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  width: 270px; /*test 250px */
}

a:hover, a:active {
  background-color: red;
}

article {
  float: right;
  padding: 20px;
  width: 50%; /* 70% */ 
  background-color: #d2dcf3;
  height: 350px; /* only for demonstration, should be removed */
  background-image: url(uitm.jpg);
  background-repeat: no-repeat;
  background-size: 500px;
  background-position: 100%;
}

/* Clear floats after the columns */
section::after {
  content: "";
  display: table;
  clear: both;
}

/* Style the footer */
footer {
  background-color: rgb(4, 162, 255);
  padding: 10px;
  font-size: 20px;  /*test */
  text-align: center;
  color: rgb(230, 230, 230);
}

/* Responsive layout - makes the two columns/boxes stack on top of each other instead of next to each other, on small screens */
@media (max-width: 1200px) {
  nav, article {
    width: 100%;
    height: 100%;
  }
}

</style>
</head>
<body>

<header>
  <h2>SUBMISSION ASSIGNMENT SYSTEM</h2>
</header>
 
<section>
  <nav>
      <H3 style="color: #373b42;">LOG IN AS :</H3>
      <a href="student_login.php" target="_self">STUDENT</a> 
      
      <a href="lecturer_login.php" target="_self">LECTURER</a> 
      <br>
      <br>
      <h1 style="color:DodgerBlue;"  >WELCOME TO SUBMISSION ASSIGNMENT SYSTEM</h1>
  </nav>
  
  <article>
    <h1 style="color:DodgerBlue;"  ></h1>  
  </article>
</section>

<footer>
  <p>UiTM CAWANGAN TERENGGANU KAMPUS KUALA TERENGGANU</p>
</footer>

</body>
</html>
