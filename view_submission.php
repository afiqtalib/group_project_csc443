<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VIEW SUBMISSION</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 400px 400px;
            row-gap: 5%;
        }
        body {
            background-color: #a6eebb8a;
        }

    </style>
    <?php 
    error_reporting(0);
    include 'con_to_db.php';
    session_start();
    $id = $_GET["goto"]
    ?>
</head>
<body>
<?php include 'lect_nav.php'; ?>
<br><br>
    <h2>SUBMISSION</h2>
    <div class="grid-container">
        <form action="sub_detail.php">
            <label for="goto">GO TO ASSIGNMENT: </label>
            <div>    
                <select id="goto" name="goto">
                    <option value="0" disabled selected>SELECT GROUP NUMBER</option> 
                    <?php 
                    $result = mysqli_query($conn,"select sub_id , GP_NUM from submission where ASS_ID ='$id'");
                    while($row = mysqli_fetch_row($result)) {
                        $i = 0;
                        foreach ($row as $cell) {
                            if($i % 2 == 0) {
                                 echo "<option value=\"$cell\"";
                               
                            }
                            else echo ">".strtoupper($cell)."</option>";
                            $i++;
                        }
                    } ?>
                </select>
                <input type="submit">
            </div>
        </form>
    </div>
</body>
</html>
