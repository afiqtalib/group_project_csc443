<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do</title>
    <style>
        th {  
            text-align: center;
        }
        td {
            text-align: center;
            border-style: solid;
            
        }
        body {
            background-color: #a6eebb8a;
        }

    </style>
    <?php 
    error_reporting(0);
        include 'con_to_db.php';
        session_start();
        $id = $_SESSION["uid"];
        //fetch class
        $result = mysqli_query($conn,"select  STD_CLASS FROM student where std_matric_no = '$id'");
        while($row=mysqli_fetch_assoc($result))  {
            $kelas = $row["STD_CLASS"];
        }
        mysqli_free_result($result);

    ?>
</head>
<body>
<?php include 'student_nav.php'; ?>
<br><br> <br>
    <table  >
        <th>No.</th>
        <th>ASSIGNMENT NAME</th>
        <th>DUE DATE</th>
        <?php 
            $j = 1;
            $result = mysqli_query($conn,"select ASS_NAME, ASS_DUE_DATE FROM assignment where CLASS_Num = '$kelas'");
            while($row = mysqli_fetch_row($result)) {
                $i = 0;
                foreach ($row as $cell) {
                    if($i % 2 == 0) {

                        echo "<tr><td>". ($j++) ."</td>";
                        
                        echo "<td>".strtoupper($cell)."</td>";
                    }
                    else echo "<td>$cell</td></tr>";
                    $i++; 
                    
                }} 
        
        ?>
    </table>
    <br>
    <form action="view_ass.php">
    <label for="goto">GO TO ASSIGNMENT: </label>
                        <div>    
                            <select id="goto" name="goto">
                                <option value="0" disabled selected>SELECT ASSIGNMENT NAME</option> 
                                <?php $result = mysqli_query($conn,"select ASS_ID, ASS_NAME FROM assignment where CLASS_Num = '$kelas'");
                        while($row = mysqli_fetch_row($result)) {
                            $i = 0;
                            foreach ($row as $cell) {
                                if($i % 2 == 0) {
                                    if (strcmp($cell, $id) != 0) echo "<option value=\"$cell\"";
                                    else $i+= 2;
                                }
                                else echo ">".strtoupper($cell)."</option>";
                                $i++;
                            }} ?>
                            </select>
                            <input type="submit">
                        </div>

    </form>
</body>
</html>
