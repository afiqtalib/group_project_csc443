<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
    include 'con_to_db.php';
    error_reporting(0);
    session_start();
    $id = $_SESSION["uid"];

    $result = mysqli_query($conn,"select STD_NAME, STD_CLASS FROM student where std_matric_no = '$id'");
    while($row=mysqli_fetch_assoc($result))  {
        $name1 = $row["STD_NAME"];    
        $kelas = $row["STD_CLASS"];
    }
    mysqli_free_result($result);
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Group</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 120px 200px;
            row-gap: 5%;
        }
        body {
            background-color: #a6eebb8a;
        }

    </style>
</head>
<body>
<?php include 'student_nav.php'; ?>
<br><br>
    <h1>Create Group</h1>
    <form action="cr_pg_handle.php" method="POST" >
        <div class="grid-container">

        <label for="gpname">Group Name: </label>
        <input type="text" name="gpname" id="gpname" required>
        <input type="hidden" name="kelas" value="<?php echo $kelas; ?>" > 
        
    <label > Group Leader: </label>
    <Label ><?php echo $name1?></Label>
    <input type="hidden" name="lead" value="<?php echo $id;?>">
    
    <label for="member1">Member 1:</label>
    <div>    
        <select id="member1" name="member1">
            <option value="0" disabled selected>SELECT MEMBER</option>
            <option value="0" >NO MEMBER</option>  
            <?php 
                        $result = mysqli_query($conn,"select STD_MATRIC_NO, STD_NAME FROM student where STD_CLASS = '$kelas'");
                        while($row = mysqli_fetch_row($result)) {
                            $i = 0;
                            foreach ($row as $cell) {
                                if($i % 2 == 0) {
                                    if (strcmp($cell, $id) != 0) echo "<option value=\"$cell\"";
                                    else $i+= 2;
                                }
                                else echo ">".strtoupper($cell)."</option>";
                                $i++;
                            }} ?>
                            </select>
                        </div>
                        
                        <label for="member2">Member 2: </label>
                        <div>    
                            <select id="member2" name="member2">
                                <option value="0" disabled selected>SELECT MEMBER</option> 
                                <option value="0" >NO MEMBER</option>  
                                <?php $result = mysqli_query($conn,"select STD_MATRIC_NO, STD_NAME FROM student where STD_CLASS = '$kelas'");
                        while($row = mysqli_fetch_row($result)) {
                            $i = 0;
                            foreach ($row as $cell) {
                                if($i % 2 == 0) {
                                    if (strcmp($cell, $id) != 0) echo "<option value=\"$cell\"";
                                    else $i+= 2;
                                }
                                else echo ">".strtoupper($cell)."</option>";
                                $i++;
                            }} ?>
                            </select>
                        </div>
                        
                        <label for="member3">Member 3: </label>
                        <div>    
                            <select id="member3" name="member3">
                                <option value="0" disabled selected>SELECT MEMBER</option>
                                <option value="0" >NO MEMBER</option>   
                                <?php $result = mysqli_query($conn,"select STD_MATRIC_NO, STD_NAME FROM student where STD_CLASS = '$kelas'");
                        while($row = mysqli_fetch_row($result)) {
                            $i = 0;
                            foreach ($row as $cell) {
                                if($i % 2 == 0) {
                                    if (strcmp($cell, $id) != 0) echo "<option value=\"$cell\"";
                                    else $i+= 2;
                                }
                                else echo ">".strtoupper($cell)."</option>";
                                $i++;
                            }} ?>
                            </select>
                        </div>
                        
                        <input type="submit" style="margin-top: 10px;">
                    </div>
                </form>
            </body>
            </html>
            
