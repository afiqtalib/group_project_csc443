<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submission</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 200px 400px;
            row-gap: 5%;
        }
        .MISSING {
            color: red;
        }
        .SUBMITTED {
            color: green;
        }
        .GRADED {
            color: green;
        }
        body {
            background-color: #a6eebb8a;
        }

    </style>
    <?php
    error_reporting(0); 
     include 'con_to_db.php';
    //fetch
    session_start();
    $result = mysqli_query($conn,"select * FROM assignment where ASS_ID = '$_GET[goto];'");
    while($row = mysqli_fetch_assoc($result)) {
        $id = $row["ASS_ID"];
        $name = $row['ASS_NAME'];
        $desc = $row["ASS_DESCRIPTION"];
        $duedate = $row["ASS_DUE_DATE"];
        $location = $row["ASS_ATTACHMENT"];
    }
    mysqli_free_result($result);
    ?>
</head>
<body>
<?php include 'student_nav.php'; ?>
<br><br>
    <h3>ASSIGNMENT DETAILS</h3>
    <div class="grid-container">
        <label >ASSIGNMENT NAME:</label>
        <label ><?php echo strtoupper($name);?> </label>

        <label >ASSIGNMENT DESCRIPTION:</label>
        <label ><?php echo "$desc";?></label>

        <label > DUE ON:</label>
        <label > <?php echo "$duedate";?></label>

        <label >ATTACHMENT:</label>
        <?php 
        if (strcmp($location, "NO ATTACHMENT") == 0) {
            echo "<label >NO ATTACHMENT</label>";
        }
        else {
            echo "<a href=\"uploads/$location\" target=\"_blank\">View file</a>";
        }
        ?>
        
        

        <label >STATUS</label>           
            
            <?php 
            $status = "MISSING";
            //fetch submission status
            $result = mysqli_query($conn,"SELECT  sub.SUB_STATUS, sub.SUB_ATTACHMENT
                                            from submission sub join group_assignment gp
                                            on gp.GP_NUM = sub.GP_NUM
                                            where sub.ASS_ID = $id
                                            and gp.STD_MATRIC_NO = $_SESSION[uid]");
            while($row = mysqli_fetch_row($result)) {
                $value = $row[0];
                    if (strcmp($value, "SUBMITTED") == 0 || strcmp($value, "GRADED") == 0 ) $status = $value ;
                $sub_location = $row[1];
            }
            mysqli_free_result($result);
            ?>
        <label class="<?php echo "$status\">$status"; ?></label>
        
        <?php  
        if ($status == "MISSING") {echo /*html*/"
        <label >SUBMIT WORK</label>
        <form action=\"submission.php\" method=\"POST\" >
        <input type=\"hidden\" name=\"ass_id\" value=\"$id\">
        <input type=\"hidden\" name=\"due_date\" value=\"$duedate\">
        <input type=\"hidden\" name=\"name\" value=\"$name\">
        <button>Submit</button>
        </form>";}

        else { 
            echo /*html*/"<label> VIEW WORK: </label>";
            echo "<a href=\"uploads/$sub_location\" target=\"_blank\">View file</a>";
            if ($value == "GRADED") {
                $mark = "an error had occured";
                $result = mysqli_query($conn, "select SUB_GRADED_MARK from submission where STD_MATRIC_NO = $_SESSION[uid] and ASS_ID = $id");
                while ($row = mysqli_fetch_assoc($result)) {
                    $mark = $row["SUB_GRADED_MARK"];

                }
           
                echo /*html*/"<label> MARKS: </label>
                                <label> $mark </label>";
            }

        }
        ?>
        
    </div>
</body>
</html>