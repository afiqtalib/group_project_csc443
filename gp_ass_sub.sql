-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 30, 2021 at 04:08 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gp_ass_sub`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE `assignment` (
  `ASS_ID` int(11) NOT NULL,
  `ASS_NAME` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ASS_DESCRIPTION` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ASS_ATTACHMENT` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ASS_CREATED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ASS_DUE_DATE` timestamp NOT NULL,
  `LECT_ID` int(11) NOT NULL,
  `CLASS_NUM` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `CLASS_ID` int(11) NOT NULL,
  `CLASS_NAME` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LECT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_assignment`
--

CREATE TABLE `group_assignment` (
  `Row_ID` int(11) NOT NULL,
  `GP_NUM` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GP_NAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CLASS_NUM` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STD_MATRIC_NO` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecturer`
--

CREATE TABLE `lecturer` (
  `STAFF_ID` int(11) NOT NULL,
  `LECT_NAME` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LECT_EMAIL` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LECT_PASSWORD` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LECT_PHONE` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `STD_MATRIC_NO` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STD_NAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STD_EMAIL` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STD_PASSWORD` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STD_PHONE` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STD_CLASS` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `submission`
--

CREATE TABLE `submission` (
  `SUB_ID` int(11) NOT NULL,
  `SUB_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SUB_ATTACHMENT` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SUB_GRADED_MARK` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SUB_STATUS` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GP_NUM` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'GROUP STUDENT BELONG TO',
  `STD_MATRIC_NO` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'STUDENT WHO SUBMIT',
  `ASS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignment`
--
ALTER TABLE `assignment`
  ADD PRIMARY KEY (`ASS_ID`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`CLASS_ID`);

--
-- Indexes for table `group_assignment`
--
ALTER TABLE `group_assignment`
  ADD PRIMARY KEY (`Row_ID`),
  ADD KEY `GP_NUM_INDEX` (`GP_NUM`);

--
-- Indexes for table `lecturer`
--
ALTER TABLE `lecturer`
  ADD PRIMARY KEY (`STAFF_ID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`STD_MATRIC_NO`);

--
-- Indexes for table `submission`
--
ALTER TABLE `submission`
  ADD PRIMARY KEY (`SUB_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignment`
--
ALTER TABLE `assignment`
  MODIFY `ASS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `CLASS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_assignment`
--
ALTER TABLE `group_assignment`
  MODIFY `Row_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecturer`
--
ALTER TABLE `lecturer`
  MODIFY `STAFF_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `submission`
--
ALTER TABLE `submission`
  MODIFY `SUB_ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
