<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submission</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 200px 400px;
            row-gap: 5%;
        }
        .MISSING {
            color: red;
        }
        .SUBMITTED {
            color: green;
        }
        .GRADED {
            color: green;
        } 
        body {
            background-color: #b8e8f0a1 ;
        }

    </style>
    <?php
    error_reporting(0); 
     include 'con_to_db.php';
    //fetch submission
    session_start();
    $result = mysqli_query($conn,"select * FROM submission where sub_id = '$_GET[goto];'");
    while($row = mysqli_fetch_assoc($result)) {
        $id = $row["SUB_ID"];
        $date = $row['SUB_DATE'];
        $gp_num = $row["GP_NUM"];
        $leader = $row["STD_MATRIC_NO"];
        $location = $row["SUB_ATTACHMENT"];
        $mark = $row["SUB_GRADED_MARK"];
    }
    mysqli_free_result($result);

    ?>
</head>
<body>
<?php include 'lect_nav.php'; ?>
<br><br>
    <h3>ASSIGNMENT DETAILS</h3>
    <form action="update_mark.php" method="post">
    <div class="grid-container">

            <?php echo "
        <label > SUBMITTED ON:<br><br></label>
        <label > $date </label>";
        
        echo "<label >GROUP MEMBERS:</label>
        <label >" ;
        
        //fetch student
        $result = mysqli_query($conn,"SELECT DISTINCT concat(gp.STD_MATRIC_NO, \"   \", 
            upper(st.STD_NAME)) FROM group_assignment gp 
            join student st on gp.STD_MATRIC_NO = st.STD_MATRIC_NO 
            WHERE gp.GP_NUM = '$gp_num'; ");

while($row = mysqli_fetch_row($result)) {
    foreach ($row as $value) {
        echo "$value <br>";
    }
}


echo "</label><label>UPLOADED FILE:</label>";


if (strcmp($location, "NO ATTACHMENT") == 0) {
    echo "<label >NO ATTACHMENT</label>";
}
else {
    echo "<a href=\"uploads/$location\" target=\"_blank\">View file</a>";
}
?>       
        
        <label for="mark" >MARKS</label>
        <input type="text" name="mark" id="mark" style="width: 100px;">
        <label>GRADE ASSIGNMENT</label>
        <input type="hidden" name="sub_id" value="<?php echo $id; ?>">           
        <input type="submit" style="width: 100px;" >
        
    </div>
</form>
</body>
</html>