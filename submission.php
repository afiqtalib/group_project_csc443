<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submission</title>
    <style>
        .grid-container {
            margin-left: 5%;
            display: grid;
            grid-template-columns: 160px 250px;
            row-gap: 5%;
            align-items: center;
        }
        body {
            background-color: #a6eebb8a;
        }


    </style>
    <?php 
    error_reporting(0);
    include 'con_to_db.php';
    session_start();
    ?>
</head>
<body>
<?php include 'lect_nav.php'; ?>
<br><br>
    <h3>SUBMISSION</h3>
    <form action="submit_handle.php" method="post" enctype="multipart/form-data" >
            <div class="grid-container">

            <label >ASSIGNMENT NAME:</label>
            <label ><?php echo strtoupper($_POST["name"]);?> </label>
            
            <label > DUE ON: </label>
            <label > <?php echo $_POST["due_date"];?></label>
            
            <label for="file">UPLOAD ATTACHMENT:</label>
            <input type="file" id="file" name="file" required>
        
            <label for="gp_num">GROUP NAME</label>
            <select id="gp_num" name="gp_num" required>
                                <option value="" disabled selected>SELECT GROUP NAME</option> 
                                <?php $result = mysqli_query($conn,"select DISTINCT GP_NUM, gp_name
                                        from group_assignment where STD_MATRIC_NO = '$_SESSION[uid]'");
                        while($row = mysqli_fetch_row($result)) {
                            $i = 0;
                            foreach ($row as $cell) {
                                if($i % 2 == 0) {
                                    if (strcmp($cell, $id) != 0) echo "<option value=\"$cell\"";
                                    else $i+= 2;
                                }
                                else echo ">".strtoupper($cell)."</option>";
                                $i++;
                            }} ?>
                            </select>

            <input type="hidden" name="ass_id" value="<?php echo $_POST["ass_id"] ; ?>">
            <label >SUBMIT WORK</label>
            <input type="submit" >        
        </div>
        </form>
</body>
</html>