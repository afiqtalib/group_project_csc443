<!DOCTYPE html>
<html lang="en">
<head>
<style>
    ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  position: fixed;
  top: 0;
  width: 100%;
  border: 1px solid #e7e7e7;
  background-color: #8edda5;
}


li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* Change the link color to #111 (black) on hover */
li a:hover {
  background-color: #111;
  border-right: 1px solid #bbb;
}
li:last-child {
  border-right: none;
}
.active {
  background-color: #04AA6D;
}
</style>

</head>
<body>
<ul>
  <li><a class="active" href="studentpage.php" >Home</a></li>
  <li style="float:right"><a  class="active" href="logout.php" >Log Out</a></li>
</ul>    
</body>
</html>